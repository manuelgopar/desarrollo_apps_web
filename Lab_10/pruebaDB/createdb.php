<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "Hotel_clientes2";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 


// sql to create table
$sql = "CREATE TABLE Clientes (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
Nombre VARCHAR(30) NOT NULL,
RFC VARCHAR(30) NOT NULL,
email VARCHAR(50) NOT NULL,
Empresa VARCHAR(50) NOT NULL,
Ciudad VARCHAR(50) NOT NULL,
reg_date TIMESTAMP
)";

if ($conn->query($sql) === TRUE) {
    echo "Table Clientes created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}

$conn->close();
?>